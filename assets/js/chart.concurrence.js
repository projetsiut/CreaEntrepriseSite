var dataPoints = [
    {label:'Facebook',y:33},
    {label:'Snapchat',y:12.2},
    {label:'WhatsApp',y:13.4}
];

$(document).ready(function () {

    var chart = new CanvasJS.Chart("chart", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Utilisateur unique en france"
        },
        axisY: {
            title: "en Millions",
            titleFontSize: 24
        },
        data: [{
            type: "column",
            yValueFormatString: "#,### M Visiteurs",
            dataPoints: dataPoints
        }]
    });

    chart.render();
});