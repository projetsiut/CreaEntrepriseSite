const ninja = $('#ninja').children().eq(0);


$(document).ready(function () {
	ninja.css({
		width:'50px',
		height:'50px',
		position:'absolute',
		display:'block'
	});

	setTimeout(pop,2000);
	//console.log(ninja);
});

function pop() {
	let x = getRandomInt(window.innerHeight);
	let y = getRandomInt(window.innerWidth);

	ninja.css({
		left:x+'px',
		top:y+'px',
	}).fadeIn(500);

	setTimeout(function () {
		ninja.fadeOut(500)
	}, 1500);

	setTimeout(pop,getRandomInt(11000));
}

function getRandomInt(max) {
	return Math.floor(Math.random() * (max)) +1;
}
