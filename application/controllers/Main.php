<?php

class Main extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->main();
	}

	public function main()
	{
		$this->load->view('main/main');
	}

    public function clients()
    {
        $this->load->view('main/clients');
	}

    public function concurrence()
    {
        $this->load->view('main/concurrence');
	}

	public function emplacement()
    {
        $this->load->view('main/emplacement');
	}

	public function environnement()
    {
        $this->load->view('main/environnement');
	}

	public function aides()
    {
        $this->load->view('main/aides');
	}

	public function strategie()
    {
        $this->load->view('main/strategie');
	}

	public function finance()
    {
        $this->load->view('main/finance');
	}

	public function juridique()
    {
        $this->load->view('main/juridique');
	}

    public function fournisseurs()
    {
        $this->load->view('main/fournisseurs');
    }

    public function conclusion()
    {
        $this->load->view('main/conclusion');
    }
}
