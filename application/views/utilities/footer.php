		</main>
		<div id="ninja">
			<img src="<?= base_url('assets/img/ninja.svg') ?>" alt="">
		</div>
		<footer class="page-footer amber lighten-1">
			<div class="footer-copyright">
				<div class="container black-text">
					© 2018 Copyright Holder All Rights Reserved. With love from <a href="http://materializecss.com/">Materialize</a>. Icons from <a href="https://www.flaticon.com/">FlatIcon</a>.
				</div>
			</div>
		</footer>
		<?php
			foreach ($load as $js){
				echo '<script src="'.base_url().'assets/js/'.$js.'.js"></script>';

			}
			echo '<script src="'.base_url().'assets/js/ninja.js"></script>';
		?>
	</body>
</html>
