<div class="navbar-fixed">
	<nav class="amber lighten-1">
		<div class="nav-wrapper beer">
			<a href="acceuil" class="brand-logo black-text"><img src="<?= base_url().'assets/img/logo_.png' ?>" alt="Auzoot" width="70px"></a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="https://goo.gl/forms/MvSVycN9azYSQBDS2" class="black-text">Sondage</a></li>
				<li><a href="clients" class="black-text">Clients</a></li>
				<li><a href="concurrence" class="black-text">Concurrence</a></li>
				<li><a href="emplacement" class="black-text">Implantation</a></li>
				<li><a href="environnement" class="black-text">Environnement</a></li>
				<li><a href="strategie" class="black-text">Strategie</a></li>
				<li><a href="fournisseurs" class="black-text">Fournisseurs</a></li>
				<li><a href="aides" class="black-text">Aides</a></li>
				<li><a href="https://docs.google.com/spreadsheets/d/1sYXvCr90IjlOp-Yb5pnMCPzBuJjWqNWWvC0D69GrYuY/edit?usp=sharing" class="black-text">Finance</a></li>
				<li><a href="juridique" class="black-text">Juridique</a></li>
				<li><a href="conclusion" class="black-text">Conclusion</a></li>
			</ul>
		</div>
	</nav>
</div>
