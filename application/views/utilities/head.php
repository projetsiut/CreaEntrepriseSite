<!DOCTYPE html>
<html lang="fr">
<head>
	<!-- Base Tags -->
	<meta charset="utf-8">
	<title><?= $title ?></title>
	<meta name="author" content="Guillaume Marmorat, Jérémie Chantharath">
	<meta name="description" content="Projet création d'entreprise" />
	<link rel="icon" type="image/png" href="<?= base_url().'assets/img/logo.png' ?>">

	<!--Import Google Icon Font-->
<!--	<link rel="stylesheet" href="--><?//= base_url().'assets/css/materialIcon.css' ?><!--" >-->
	<!-- Stylesheet -->
	<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/materialize.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/main.css' ?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<main>
	<body>
