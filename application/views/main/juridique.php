<?php
$data['title'] = 'Juridique';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>

    <div class="container">
        <h2>Auzoot et le <?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <ul class="collapsible">
                <li>
                    <div class="collapsible-header"><img src="<?= base_url('assets/img/jurid/law.svg') ?>" alt="" width="30" height="24" style="margin-right: 15px">Société Anonyme (SA)</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="col s6">
                                <h4>Avantages</h4>
                                <ul class="collection">
                                    <li class="collection-item">Gage de sécurité</li>
                                    <li class="collection-item">Actions négicoables et céssibles</li>
                                    <li class="collection-item">Plus large répartition du pouvoirs</li>
                                </ul>
                            </div>
                            <div class="col s6">
                                <h4>Inconveniants</h4>
                                <ul class="collection">
                                    <li class="collection-item">Conviens mieux a un grande entreprise car trés lourd</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><img src="<?= base_url('assets/img/jurid/law.svg') ?>" alt="" width="30" height="24" style="margin-right: 15px">Société à Responsabilité Limité (SARL)</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="col s6">
                                <h4>Avantages</h4>
                                <ul class="collection">
                                    <li class="collection-item">Cadre juridique sécurisant</li>
                                    <li class="collection-item">Responsable que dans la limite de l'apport</li>
                                </ul>
                            </div>
                            <div class="col s6">
                                <h4>Inconveniants</h4>
                                <ul class="collection">
                                    <li class="collection-item">Afiliation a un particulier</li>
                                    <li class="collection-item">Dividende asujettis aux charges sociales</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><img src="<?= base_url('assets/img/jurid/law.svg') ?>" alt="" width="30" height="24" style="margin-right: 15px">Société par actions simplifiée (SAS)</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="col s6">
                                <h4>Avantages</h4>
                                <ul class="collection">
                                    <li class="collection-item">Grande libertés des associées</li>
                                    <li class="collection-item">Dirigeants assimilés salariés donc protection sociale</li>
                                </ul>
                            </div>
                            <div class="col s6">
                                <h4>Inconveniants</h4>
                                <ul class="collection">
                                    <li class="collection-item">Rédaction des status nécessitant des compétences</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><img src="<?= base_url('assets/img/jurid/law.svg') ?>" alt="" width="30" height="24" style="margin-right: 15px">Auto Entrepreneur (AE)</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="col s6">
                                <h4>Avantages</h4>
                                <ul class="collection">
                                    <li class="collection-item">Simplicité</li>
                                    <li class="collection-item">Régime social</li>
                                    <li class="collection-item">Pas de TVA</li>
                                </ul>
                            </div>
                            <div class="col s6">
                                <h4>Inconveniants</h4>
                                <ul class="collection">
                                    <li class="collection-item">Attention au seuils de revenues</li>
                                    <li class="collection-item">Solution principalement temporaire</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

<?php
$data['load'] = array('jquery','materialize', 'collapsible');
$this->load->view('utilities/footer',$data);

