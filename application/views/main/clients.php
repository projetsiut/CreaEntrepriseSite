<?php
$data['title'] = 'Clients';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>

<!-- Modal displayed on button clic hidden else-->
    <div id="modal1" class="modal">
        <div class="modal-content center-align">
            <h4>Chiffres clé</h4>
            <img src="<?= base_url('assets/img/client/revenus.png') ?>" alt="Chiffre clé">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>
    <div id="modal2" class="modal">
        <div class="modal-content center-align">
            <h4>Chiffres clé</h4>
            <img src="<?= base_url('assets/img/client/evolution.png') ?>" alt="Chiffre clé">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>
    <div id="modal3" class="modal">
        <div class="modal-content center-align" style="padding-bottom: 0px">
            <h4>La demande</h4>
            <div class="slider">
                <ul class="slides">
                    <li>
                        <img src="<?= base_url('assets/img/client/retours.png') ?>">
                    </li>
                    <li>
                        <img src="<?= base_url('assets/img/client/fonction.png') ?>">
                    </li>
                </ul>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>
    <div id="modal4" class="modal">
        <div class="modal-content center-align row">
            <h4>L'offre d'Auzoot</h4>
            <ul class="tabs">
                <li class="tab col s4"><a class="active" href="#new">Nouveau</a></li>
                <li class="tab col s4"><a href="#tech">Moderne</a></li>
                <li class="tab col s4"><a href="#more">Le plus</a></li>
            </ul>
            <div id="new" class="col s10 offset-s1" style="padding-top: 25px"><h4>Il n'éxiste pas encore de site spécialisé sur ce marché, Auzoot est donc un concept novateur.</h4></div>
            <div id="tech" class="col s10 offset-s1" style="padding-top: 25px"><h4>Les dernieres technologies sont en place au service de l'utilisateur, chez Auzoot on aime les choses belles.</h4></div>
            <div id="more" class="col s10 offset-s1" style="padding-top: 25px"><h4>La jeunesse étant de plus en plus high-tech et la fête étant universelle, la clientèle ne peut qu'augmenter.</h4></div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>


<!-- Main container-->
<div class="container">
    <h2>Auzoot et la clientèle</h2>

    <div class="row">
        <div class="card col s8 offset-s2">
<!--            Add beer class for beer bubble effect-->
            <div class="card-content amber lighten-1 beer">
                <h3>Le marché</h3>
            </div>
            <div class="card-tabs">
                <ul class="tabs tabs-fixed-width">
                    <li class="tab"><a href="#type">Type</a></li>
                    <li class="tab"><a href="#niv">Niveau</a></li>
                    <li class="tab"><a href="#sais">Saisonnalité</a></li>
                </ul>
            </div>
            <div class="card-content grey lighten-4">
                <div id="type"><h4>Marché du loisir.</h4></div>
                <div id="niv"><h4>National, puis international<br>(version en anglais du site)</h4></div>
                <div id="sais"><h4>Nous pensons que le site aura plus de fréquentations durant les périodes scolaires.</h4></div>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="card col s6">
            <div class="card-content">
                <span class="card-title">Revenus possibles</span>
                <p>Estimation du montant mensuel des revenus possibles pour différents type de rémuneration utilisés</p>
            </div>
            <div class="card-action">
<!--                Open modal1 on click-->
                <a href="#modal1" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
            </div>
        </div>
        <div class="card col s6">
            <div class="card-content">
                <span class="card-title">Taux de croissance</span>
                <p>Evolution des revenus sur la publicité en ligne en France</p>
            </div>
            <div class="card-action">
                <a href="#modal2" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="card col s6">
            <div class="card-content">
                <span class="card-title">La demande</span>
                <p>La demande actuelle selon le sondage et les fonctionnalitées plébiscitées</p>
            </div>
            <div class="card-action">
                <a href="#modal3" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
            </div>
        </div>
        <div class="card col s6">
            <div class="card-content">
                <span class="card-title">L'offre</span>
                <p>L'offre fournis par Auzoot</p>
            </div>
            <div class="card-action">
                <a href="#modal4" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
            <h2>La clientèle</h2>
            <div class="col s4 center">
                <i class="material-icons large amber-text">trending_up</i>
                <h5>Clientèle principalement jeunes (17-24ans) environ 80% des utilisateurs</h5>
            </div>
            <div class="col s4 center">
                <i class="material-icons large amber-text">settings_ethernet</i>
                <h5>Les parts de la population encore trop jeunes qui ne font pas encore de soirée, ou un peu plus vieille (qui n’en fait plus)</h5>
            </div>
            <div class="col s4 center">
                <i class="material-icons large amber-text">cancel</i>
                <h5>Parts de la population n’ayant pas accès à internet.</h5>
            </div>
    </div>
</div>


<?php
// Don't forget to load materialize related javascript and every module needed on the page
$data['load'] = array('jquery','materialize','tabs', 'modal', 'slider');
// Module loaded in footer, pass $data in argument to load the defined module array
$this->load->view('utilities/footer',$data);
