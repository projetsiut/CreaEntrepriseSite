<?php
$data['title'] = 'Conclusion';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>
    <style>
        [class*="button"] {
            line-height: 90px;
            font-size: 50px;
            font-family: Raleway;
            margin-top: 55px;
            background-color: #ff0066;
            border-radius: 100px;
            padding: 0;
            left: 50%;
            margin-left: -60px;
            border-bottom: 30px solid #990033;
            position: absolute;
            width: 120px;
            height: 85px;
            transition: 0.5s;
            text-align: center;
        }
        .button:active {
            margin-top: 85px;
            border-bottom: 0px solid black;
            transition: 0.25s;
            /* animation: chaos 1s; */
        }
        .button a {
            text-decoration: none;
            color: white;
        }
        .button1 {
            background-color: Gainsboro;
            width: 150px;
            height: 100px;
            border-bottom: 20px solid Grey;
            z-index: -1;
            left: 50%;
            margin-left: -75px;
            margin-top: 75px;
            animation: null 1s;
        }
    </style>
    <div class="container">
        <h2><?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <div class="row">
                <h3>On a tout pour réussir !</h3>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/aide/fiable.svg') ?>" alt="Kickstarter" width="90px" height="90px">
                    <h4>Une projet Fiable</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/aide/idea.svg') ?>" alt="Ulule" width="90px" height="90px">
                    <h4>Un projet Innovant</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/aide/strong.svg') ?>" alt="KissKiss BankBank" width="90px" height="90px">
                    <h4>Un projet Durable</h4>
                </div>
            </div>
        </div>
        <h3>Le mot de la fin</h3>
        <div class="button tooltipped" data-position="bottom" data-delay="50" data-tooltip="DON'T CLICK" onClick="sound()"> <a style="color: #ff0066;"></a> </div>
        <div class="button1"></div>
        <div style="width:50%;">
            <canvas id="canvas"></canvas>
        </div>
    </div>
    <script>
        function sound() {
            var audio = new Audio('../../assets/sound/goat.mp3');
            audio.play();
        }
    </script>

<?php
$data['load'] = array('jquery','materialize', 'tooltip');
$this->load->view('utilities/footer',$data);

