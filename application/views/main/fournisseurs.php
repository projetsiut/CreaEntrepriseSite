<?php
$data['title'] = 'Fournisseurs';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>
    <div id="modal1" class="modal">
        <div class="modal-content center-align">
            <div class="row">
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/ldlc.png') ?>" alt="LDLC">
                </div>
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/grosbill.png') ?>" alt="Grosbill">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal2" class="modal">
        <div class="modal-content center-align">
            <img src="<?= base_url('assets/img/fourn/screen.png') ?>" alt="Screen" class="col s4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Asus P348 ROG">
            <img src="<?= base_url('assets/img/fourn/mouse.png') ?>" alt="Mouse" class="col s4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Logitech MX Master 2S">
            <img src="<?= base_url('assets/img/fourn/keyb.png') ?>" alt="Keyboard" class="col s4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Corsais K70 Lux">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal3" class="modal">
        <div class="modal-content center-align">
             <table>
                 <thead>
                    <tr>
                        <th></th>
                        <th>LDLC Pro</th>
                        <th>Grosbill Pro</th>
                    </tr>
                 </thead>
                 <tbody>
                    <tr>
                        <th>Ecran</th>
                        <td>916.63€</td>
                        <td>1000.00€</td>
                    </tr>
                    <tr>
                        <th>Clavier</th>
                        <td>149.96€</td>
                        <td>149.91€</td>
                    </tr>
                    <tr>
                        <th>Souris</th>
                        <td>83.29€</td>
                        <td>91.58€</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <th>1149.88€</th>
                        <th>1241.49</th>
                    </tr>
                 </tbody>
             </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal4" class="modal">
        <div class="modal-content center-align">
            <img src="<?= base_url('assets/img/fourn/fixe.png') ?>" alt="PC Fixe" class="col s4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Homemade PC">
            <img src="<?= base_url('assets/img/fourn/portable.png') ?>" alt="PC Portable" class="col s4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Asus VivoBook Pro">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal5" class="modal">
        <div class="modal-content center-align">
            <table>
                <thead>
                <tr>
                    <th></th>
                    <th>LDLC Pro</th>
                    <th>Grosbill Pro</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Intel Core i7-8700K</th>
                    <td>349.96€</td>
                    <td>349.92</td>
                </tr>
                <tr>
                    <th>ASUS PRIME Z370-A</th>
                    <td>154.13€</td>
                    <td>150.00€</td>
                </tr>
                <tr>
                    <th>HyperX Fury Noir 16 Go</th>
                    <td>33.29€</td>
                    <td>29.16€</td>
                </tr>
                <tr>
                    <th>Samsung SSD 960 EVO M.2 PCIe</th>
                    <td>108.29€</td>
                    <td>124.91€</td>
                </tr>
                <tr>
                    <th>be quiet! Pure Rock</th>
                    <td>180.79€</td>
                    <td>184.96€</td>
                </tr>
                <tr>
                    <th>Corsair RM650x 80PLUS Gold</th>
                    <td>94.13€</td>
                    <td>94.13€</td>
                </tr>
                <tr>
                    <th>Corsair Carbide 400C Windowed</th>
                    <td>86.63€</td>
                    <td>86.58€</td>
                </tr>
                <tr>
                    <th>Asus VivoBook Pro</th>
                    <td>1333.29€</td>
                    <td>1333.26</td>
                </tr>
                <tr>
                    <th>Total</th>
                    <th>2340€</th>
                    <th>2352€</th>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal6" class="modal">
        <div class="modal-content center-align">
            <div class="row">
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/franceb.png') ?>" alt="France Bureau">
                </div>
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/scandd.png') ?>" alt="Scandinavian Design">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal7" class="modal">
        <div class="modal-content center-align">
            <img src="<?= base_url('assets/img/fourn/chair.png') ?>" alt="Screen" class="col s6 tooltipped" data-position="bottom" data-delay="50" data-tooltip="WAU Desk Chair">
            <img src="<?= base_url('assets/img/fourn/desk.png') ?>" alt="Mouse" class="col s6 tooltipped" data-position="bottom" data-delay="50" data-tooltip="Network Sit Stand Desk">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal8" class="modal">
        <div class="modal-content center-align">
            <table>
                <thead>
                <tr>
                    <th></th>
                    <th>France Bureau</th>
                    <th>Scandinavian Design</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Bureau</th>
                    <td>802.20€</td>
                    <td>499€</td>
                </tr>
                <tr>
                    <th>Chaise</th>
                    <td>NC</td>
                    <td>499€</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal9" class="modal">
        <div class="modal-content center-align">
            <div class="row">
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/ovh.png') ?>" alt="OVH">
                </div>
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/pulse.png') ?>" alt="Pulse Heberg">
                </div>
                <div class="col s6">
                    <img src="<?= base_url('assets/img/fourn/1et1.png') ?>" alt="1&1">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal10" class="modal">
        <div class="modal-content center-align">
            <h5>Nom de domaine : auzoot.beer</h5>
            <h5>Hebergement : serveur dédié et administrées</h5>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div id="modal11" class="modal">
        <div class="modal-content center-align">
            <table>
                <thead>
                <tr>
                    <th></th>
                    <th>OVH</th>
                    <th>PulseHberg</th>
                    <th>1&1</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Nom de domaine</th>
                    <td>23.99€</td>
                    <td>19.99€</td>
                    <td>Non Disponible</td>
                </tr>
                <tr>
                    <th>Hebergement</th>
                    <td>64.99€ HT</td>
                    <td>69.99€ HT</td>
                    <td>51.99€ HT</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div class="container">
        <h2>Auzoot et les <?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <h3>Type d'achat</h3>
            <div class="col s4 center">
                <img src="<?= base_url('assets/img/fourn/server.svg') ?>" alt="Hardware" width="90px" height="90px">
                <h4>Ordinateurs, mobiliers</h4>
            </div>
            <div class="col s4 center">
                <img src="<?= base_url('assets/img/fourn/domain.svg') ?>" alt="Virtual" width="90px" height="90px">
                <h4>Nom de domaine</h4>
            </div>
            <div class="col s4 center">
                <img src="<?= base_url('assets/img/fourn/virtual.svg') ?>" alt="Service" width="90px" height="90px">
                <h4>Hebergement, référencement</h4>
            </div>
        </div>
        <div class="row">
            <h3>Materiel</h3>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Les fournisseurs</span>
                    <p>Les fournisseurs retenus</p>
                </div>
                <div class="card-action">
                    <a href="#modal1" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                </div>
            </div>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Les périphériques</span>
                    <p>Les périphériques retenus pour un travaille plus efficace</p>
                </div>
                <div class="card-action">
                    <a href="#modal2" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                    <a href="#modal3" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Comparatif</a>
                </div>
            </div>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Le harware</span>
                    <p>Le hardware retenus pour travailler</p>
                </div>
                <div class="card-action">
                    <a href="#modal4" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                    <a href="#modal5" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Comparatif</a>
                </div>
            </div>
        </div>
        <div class="row">
            <h3>Mobilier</h3>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Les fournisseurs</span>
                    <p>Les fournisseurs retenus</p>
                </div>
                <div class="card-action">
                    <a href="#modal6" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                </div>
            </div>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Le mobilier</span>
                    <p>Le mobilier retenus pour un travaille plus agréable</p>
                </div>
                <div class="card-action">
                    <a href="#modal7" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                    <a href="#modal8" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Comparatif</a>
                </div>
            </div>
        </div>
        <div class="row">
            <h3>Virtuel</h3>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Les fournisseurs</span>
                    <p>Les fournisseurs retenus</p>
                </div>
                <div class="card-action">
                    <a href="#modal9" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                </div>
            </div>
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Les services</span>
                    <p>Les service retenus</p>
                </div>
                <div class="card-action">
                    <a href="#modal10" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                    <a href="#modal11" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Comparatif</a>
                </div>
            </div>
        </div>
    </div>

<?php
$data['load'] = array('jquery','materialize', 'modal', 'tooltip');
$this->load->view('utilities/footer',$data);

