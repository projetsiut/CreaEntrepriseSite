<?php
$data['title'] = 'Environnement';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>

    <div class="container">
        <h2>Auzoot et l'<?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header"><i class="material-icons">border_all</i>Tableau PESTEL, explications</div>
                    <div class="collapsible-body center"><img src="<?= base_url('assets/img/envir/pestel.png') ?>" alt="PESTEL"></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">settings_applications</i>Notre application de PESTEL</div>
                    <div class="collapsible-body">
                        <table>
                            <thead>
                                <tr>
                                    <th>Politique</th>
                                    <th>Economique</th>
                                    <th>Social</th>
                                    <th>Technologique</th>
                                    <th>Environnemental</th>
                                    <th>Légal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>X</td>
                                    <td>X</td>
                                    <td>Suivre les tendances de notre clientèle quant a notre marché</td>
                                    <td>Suivre l'évolution des technologies web pour avoir un site toujours plus compétitif</td>
                                    <td>Avoir une gestion des serveurs éfficace</td>
                                    <td>Réglementation sur les droit des TIC</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">border_all</i>Tableau SWOT, explications</div>
                    <div class="collapsible-body center"><img src="<?= base_url('assets/img/envir/swot.png') ?>" alt="SWOT"></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">settings_applications</i>Notre application de SWOT</div>
                    <div class="collapsible-body">
                        <table>
                            <tr>
                                <th></th>
                                <th>Atouts</th>
                                <th>Handicaps</th>
                            </tr>
                            <tr>
                                <th>Interne</th>
                                <td>Forces :
                                    Premier site français spécialisé dans ce domaine
                                </td>
                                <td>Faiblesses :
                                    Pas beaucoup de personnel pour l’instant
                                </td>
                            </tr>
                            <tr>
                                <th>Externe</th>
                                <td>
                                    Opportunités :
                                    Technologies du web émergentes
                                    Une jeunesse de plus en plus high-tech, donc une clientèle de plus en plus importante
                                </td>
                                <td>Menaces :
                                    Problèmes d’images si des incidents ont lieu lors d’une soirée organisée grâce à notre site
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">code</i>Les technologies a suivre</div>
                    <div class="collapsible-body center">
                        <div class="card-panel amber valign-wrapper">
                            <img src="<?= base_url('assets/img/envir/framework.svg') ?>" alt="fw" width="50px" height="50px">
                            <span class="black-text"> Nouveaux frameworks (côté client et serveur)</span>
                        </div>
                        <div class="card-panel amber valign-wrapper">
                            <img src="<?= base_url('assets/img/envir/ia.svg') ?>" alt="ia" width="50px" height="50px">
                            <span class="black-text"> Utilisation d’intelligence artificielle (chatbot?) et machine learning</span>
                        </div>
                        <div class="card-panel amber valign-wrapper">
                            <img src="<?= base_url('assets/img/envir/webapp.svg') ?>" alt="wa" width="50px" height="50px">
                            <span class="black-text"> Progressive Web Apps, pour éventuellement partir vers le mobile</span>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
    </div>

<?php
$data['load'] = array('jquery','materialize', 'collapsible');
$this->load->view('utilities/footer',$data);

