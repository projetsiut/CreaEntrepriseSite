<?php
$data['title'] = 'Concurrence';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>

    <div id="modal1" class="modal">
        <div class="modal-content center-align">
            <h4>Chiffres clé</h4>
            <img src="<?= base_url('assets/img/conc/diff.png') ?>" alt="Concurrence">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div class="container">
        <h2>Auzoot et la concurrence</h2>
        <div class="row">
            <div class="divider"></div>
            <h3>Principalement les réseaux sociaux</h3>
            <div class="col s4 center">
                <img src="<?= base_url('assets/img/conc/facebook.svg') ?>" alt="FB" width="90px" height="90px">
                <h4>Groupes, évenements, ...</h4>
            </div>
            <div class="col s4 center">
                <img src="<?= base_url('assets/img/conc/messenger.svg') ?>" alt="MSG" width="90px" height="90px">
                <h4>Groupes, sondages, ...</h4>
            </div>
            <div class="col s4 center">
                <img src="<?= base_url('assets/img/conc/snapchat.svg') ?>" alt="SNAP" width="90px" height="90px">
                <h4>Groupes</h4>
            </div>
        </div>
        <div class="row">
            <div class="card col s6">
                <div class="card-content">
                    <span class="card-title">Les concurrents</span>
                    <p>Comparaison des principaux reseaux sociaux en temps d'utilisation et pourcentage d'utilisateurs
                        dans la tranche.</p>
                </div>
                <div class="card-action">
                    <a href="#modal1" class="waves-effect waves-light amber lighten-2 btn modal-trigger black-text">Afficher</a>
                </div>
            </div>
        </div>
        <div class="divider"></div>
        <div class="row">
            <h3>Historique</h3>
            <div class="col s8">
                <div id="chart" style="height: 550px;"></div>
            </div>
            <div class="col s4">
                <div class="row">
                    <h4>Revenu généré</h4>
                    <div class="sol s6"><h4>Facebook : $27M</h4></div>
                    <div class="sol s6"><h4>Snapchat : $404m</h4></div>
                </div>
            </div>
        </div>
        <div class="divider"></div>
        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s6"><a href="#force">Forces</a></li>
                    <li class="tab col s6"><a href="#faibl">Faiblesses</a></li>
                </ul>
            </div>
            <div id="force" class="col s12 center"><h4>Les concurrents possèdes deja une grande quantité d'utilisateurs et de plus habituer a se servir de leurs outils au qutidiens.</h4></div>
            <div id="faibl" class="col s12 center"><h4>Ils ne sont pas spécialisé dans le marché de la fête, contrairement à Auzoot qui propose des fonctionnalités précises dans ce domaine</h4></div>
        </div>
    </div>
<?php
$data['load'] = array('jquery', 'materialize', 'modal', 'chart.lib', 'chart.concurrence', 'tabs');
$this->load->view('utilities/footer', $data);
