<?php
$data['title'] = 'Acceuil';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>
<div class="container">
	<div class="row">
		<div class="col s12">
			<h2>Auzoot c'est quoi!?</h2>

			<div class="row">
				<div class="col s6">
					<div class="card" style="height: 400px">
						<div class="card-image waves-effect waves-block waves-light right">
							<img style="width: 350px; height: 350px;" class="activator" src="<?= base_url('assets/img/main/product.svg') ?>">
						</div>
						<div class="card-content" style="margin-top: none; padding-top: none">
							<span class="card-title activator grey-text text-darken-4">Product<i class="material-icons right">more_vert</i></span>
						</div>
						<div class="card-reveal">
							<span class="card-title grey-text text-darken-4">Product<i class="material-icons right">close</i></span>
							<p>
								<ul>
									<li><h5>Marque :</h5>Auzoot®</li>
									<li><h5>Format :</h5>Site web.</li>
									<li><h5>Fonction :</h5>Site de création de soirée permettant un rapprochement social.</li>
									<li><h5>Style :</h5>Positive management.</li>
								</ul>
							</p>
						</div>
					</div>
				</div>
				<div class="col s6">
					<div class="card" style="height: 400px">
						<div class="card-image waves-effect waves-block waves-light">
							<img style="width: 350px; height: 350px;" class="activator" src="<?= base_url('assets/img/main/price.svg') ?>">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">Price<i class="material-icons right">more_vert</i></span>
						</div>
						<div class="card-reveal">
							<span class="card-title grey-text text-darken-4">Price<i class="material-icons right">close</i></span>
							<p>
							<ul>
								<li><h5>Politique de prix :</h5>Utilisation gratuite avec publicité non-agressive/sponsors durant la navigation.</li>
								<li><h5>Sponsors :</h5>BDE , Cup Pong , Place2Beer , Influenceur tournée vers les jeunes ( Youtubeurs , Streameurs , … ).</li>
								<li><h5>Publicité :</h5>Google ad avec bandeau de publicité en bas de page ou sur-page de publicité à l’ouverture de la page.</li>
							</ul>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
					<div class="card" style="height: 400px">
						<div class="card-image waves-effect waves-block waves-light right">
							<img style="width: 350px; height: 350px;" class="activator" src="<?= base_url('assets/img/main/place.svg') ?>">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">Place<i class="material-icons right">more_vert</i></span>
						</div>
						<div class="card-reveal">
							<span class="card-title grey-text text-darken-4">Place<i class="material-icons right">close</i></span>
							<p>
							<ul>
								<li><h5>Logistique :</h5>L’entreprise étant un site Web il nous suffit de locaux (petits au départ) et d’un hébergeur (pulse heberge car il propose des sponsorings plutôt intéressant ).</li>
							</ul>
							</p>
						</div>
					</div>
				</div>
				<div class="col s6">
					<div class="card" style="height: 400px">
						<div class="card-image waves-effect waves-block waves-light">
							<img style="width: 350px; height: 350px;" class="activator" src="<?= base_url('assets/img/main/prom.svg') ?>">
						</div>
						<div class="card-content">
							<span class="card-title activator grey-text text-darken-4">Promotion<i class="material-icons right">more_vert</i></span>
						</div>
						<div class="card-reveal">
							<span class="card-title grey-text text-darken-4">Promotion<i class="material-icons right">close</i></span>
							<p>
							<ul>
								<li><h5>Ventes :</h5>Mailing , participation à des salons et évènements «jeunes».</li>
								<li><h5>Publicité :</h5>Brochures , presses papier et numérique , blog , newsletter , invités.</li>
								<li><h5>Relations publiques :</h5>Sponsoring , parrainage , organisation de soirée publique.</li>
							</ul>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row col s12" style="margin-top: 2%">
			<h2>Auzoot tu fais comment!?</h2>
			<div class="col s4 center">
				<i class="large material-icons amber-text">person_add</i>
				<h4>Crée ton compte</h4>
			</div>
			<div class="col s4 center">
				<i class="large material-icons amber-text">edit</i>
				<h4>Organise ta soirée</h4>
			</div>
			<div class="col s4 center">
				<i class="large material-icons amber-text">speaker</i>
				<h4>Fais la fête</h4>
			</div>
			<div class="col s12">
				<span>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda explicabo nemo officia optio rem repellendus similique ut? Aspernatur, consectetur dicta est, excepturi id illo ipsam nostrum provident, quia ratione repellat.</span><span>Ab ad alias asperiores eius esse fuga ipsam itaque magni molestiae, mollitia nesciunt odit provident quibusdam quidem sequi unde vero. Adipisci consequatur dolore eos impedit molestias quo tempora veritatis voluptate!
				</span>
			</div>
		</div>
		<div class="row">
			<h2>Auzoot c'est qui!?</h2>
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4 amber-text"><a href="#test1">Guillaume Marmorat</a></li>
					<li class="tab col s4 amber-text"><a href="#test2">Jérémie Chantharath</a></li>
					<li class="tab col s4 amber-text"><a href="#test3">Rayan Barama</a></li>
				</ul>
			</div>
			<div id="test1" class="col s12">
				<h3><i class="material-icons medium">golf_course</i> Guillaume Marmorat </h3>
				<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem deleniti et exercitationem in quasi rem. A accusantium eos id iure laudantium, minima, necessitatibus quas quidem rem repellendus, saepe similique tenetur?</span>
			</div>
			<div id="test2" class="col s12">
				<h3><i class="material-icons medium">storage</i> Jérémie Chantharath </h3>
				<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem deleniti et exercitationem in quasi rem. A accusantium eos id iure laudantium, minima, necessitatibus quas quidem rem repellendus, saepe similique tenetur?</span>
			</div>
			<div id="test3" class="col s12">
				<h3><i class="material-icons medium">free_breakfast</i> Rayan Barama</h3>
				<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem deleniti et exercitationem in quasi rem. A accusantium eos id iure laudantium, minima, necessitatibus quas quidem rem repellendus, saepe similique tenetur?</span>
			</div>
		</div>
	</div>
</div>
<?php
$data['load'] = array('jquery','materialize','tabs');
$this->load->view('utilities/footer',$data);
