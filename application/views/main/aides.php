<?php
$data['title'] = 'Aides';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>
    <style>
        .card{
            margin-bottom: 0;
        }
    </style>
    <div id="modal1" class="modal">
        <div class="modal-content center-align">
            <img src="<?= base_url('assets/img/aide/entreprendre.png') ?>" alt="Reseau entreprendre">
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div class="container">
        <h2>Auzoot et les <?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <h3>Les aides de l'état</h3>
            <div class="row">
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">Centre d'Information sur la Prévention des difficultés des entreprises (CIP)</span>
                        <p>Sensibles aux problèmes économiques, aide à l’anticipation des difficultés, fais connaître les outils de prévention offerts par la loi</p>
                    </div>
                </div>
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">Réseau entreprendre</span>
                        <p>Accompagnement financier par palier</p>
                    </div>
                    <div class="card-action">
                        <a href="#modal1" class="waves-effect grey btn modal-trigger black-text">Voir les palier</a>
                    </div>
                </div>
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">Nouvel accompagnement pour la création et la reprise d'entreprise (NACRE)</span>
                        <p>Appui technique et financier, de 1 000 à 8 000 €, d'une durée de 5 ans maximum</p>
                    </div>
                </div>
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">BPIFrance</span>
                        <p>Nombreux prêts et garanties</p>
                    </div>
                </div>
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">Prêt d’honneur</span>
                        <p>Compléter les fonds propres des porteurs de projets pour leur permettre d’obtenir les financements bancaires complémentaires (jusqu’à 50000€ avec 0%)</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h3>Les aides de la région</h3>
            <div class="row">
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">Fond régional de garantie (FRG)</span>
                        <p>Mise en place de garanties renforcées sur les prêts bancaires
                        </p>
                    </div>
                </div>
                <div class="card amber lighten-1 black-text col s6">
                    <div class="card-content">
                        <span class="card-title">Aide “Je finance mon projet”</span>
                        <ul>
                            <li>- Valider la viabilité économique et financière de votre projet</li>
                            <li>- Accéder à des financements (subvention, microcrédit, prêt à taux 0%, garantie d’emprunt)</li>
                            <li>- Bénéficier d’un accompagnement dans la durée</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h3>Le financement participatif</h3>
            <div class="row">
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/aide/kick.svg') ?>" alt="Kickstarter" width="90px" height="90px">
                    <h4>KickStarter</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/aide/ulule.png') ?>" alt="Ulule" width="90px" height="90px">
                    <h4>Ulule</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/aide/kiss.png') ?>" alt="KissKiss BankBank" width="90px" height="90px">
                    <h4>KissKiss BankBank</h4>
                </div>
            </div>
        </div>
    </div>

<?php
$data['load'] = array('jquery','materialize', 'modal');
$this->load->view('utilities/footer',$data);

