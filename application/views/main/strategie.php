<?php
$data['title'] = 'Stratégie';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>

    <div class="container">
        <h2>Auzoot et la <?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <div class="row">
                <div class="divider"></div>
                <h3>Segment</h3>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/strat/boy.svg') ?>" alt="Boy" width="90px" height="90px">
                    <h4>Jeunes ados : Soirée jeux vidéos, cinéma, ...</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/strat/party.svg') ?>" alt="Student" width="90px" height="90px">
                    <h4>Lycéens, Etudiants : Soirée étudiantes, boite de nuit, ...</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/strat/restaurant.svg') ?>" alt="SNAP" width="90px" height="90px">
                    <h4>Adultes : Restaurant, ...</h4>
                </div>
            </div>
            <ul class="collapsible">
                <li>
                    <div class="collapsible-header"><img src="<?= base_url('assets/img/strat/target.svg') ?>" alt="icon" width="30" height="24" style="margin-right: 15px">Ciblage et positionnement</div>
                    <div class="collapsible-body">
                        <div class="card-panel amber valign-wrapper">
                            <img src="<?= base_url('assets/img/strat/party_black.svg') ?>" alt="Student" width="50px" height="50px" style="margin-right: 3%">
                            <h5><em>Ciblage : </em>On s’occupera en priorité de notre premier type de clientèle (clientèle “étudiante”), car c’est la plus proche de nous.</h5>
                        </div>
                        <div class="card-panel amber valign-wrapper">
                            <img src="<?= base_url('assets/img/strat/form.svg') ?>" alt="Form" width="50px" height="50px" style="margin-right: 3%">
                            <h5><em>Positionnement : </em>On se concentrera sur les  formulaires concernant “l’organisation de soirée” en priorité étant donné que c’est la fonctionnalité principale.</h5>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">check_circle</i>Facteurs clés du succès</div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div id="tech" class="col s6">
                                <h4>Aspect technologique</h4>
                                <ul class="collection">
                                    <li class="collection-item">Un site rapide au chargement</li>
                                    <li class="collection-item">Peu de bug techniques et un site toujours disponible</li>
                                    <li class="collection-item">Conforme à W3C : un site moderne se doit de respecter cette norme</li>
                                    <li class="collection-item">Un référencement optimal</li>
                                </ul>
                            </div>
                            <div id="content" class="col s6">
                                <h4>Aspect contenu</h4>
                                <ul class="collection">
                                    <li class="collection-item">Un contenu soigné</li>
                                    <li class="collection-item">Une mise à jour régulière du site</li>
                                    <li class="collection-item">Un design rassurant : L’aspect du site doit être le plus rassurant et inspirer le plus possible confiance à l’utilisateur.</li>
                                </ul>
                            </div>
                        </div>


                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">border_all</i>Les forces de PORTER, explications</div>
                    <div class="collapsible-body center"><img src="<?= base_url('assets/img/strat/porter.png') ?>" alt="Porter"></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">settings_applications</i>Notre interpretation de PORTER</div>
                    <div class="collapsible-body">
                        <table>
                            <tr>
                                <th>Menace des nouveaux entrants</th>
                                <td>Etant le premier site sur ce marché, si on suit activement les nouvelles technologies, ils ne devraient pas être un problème</td>
                            </tr>
                            <tr>
                                <th>Pouvoir de négociation des fournisseurs</th>
                                <td>Idem</td>
                            </tr>
                            <tr>
                                <th>Pouvoir de négociation des clients</th>
                                <td>Il faudra être à l’écoute des fonctionnalités souhaitées par les clients</td>
                            </tr>
                            <tr>
                                <th>Menace des produits de substitutions</th>
                                <td>Etre vigilant de ce qui se fait ailleurs pour se démarquer, voir se développer dans un autre domaine (exemple: mobile) </td>
                            </tr>
                        </table>
                    </div>
                </li>
            </ul>
        </div>

    </div>

<?php
$data['load'] = array('jquery','materialize','collapsible', 'tabs');
$this->load->view('utilities/footer',$data);

