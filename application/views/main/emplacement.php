<?php
$data['title'] = 'Emplacement';
$this->load->view('utilities/head', $data);
$this->load->view('utilities/nav');

?>

    <div class="container">
        <h2>Auzoot et l'<?= $data['title'] ?></h2>
        <div class="divider"></div>
        <div class="row">
            <h3>Le pays : l'Irlande, plus particulièrement Dublin</h3>
            <div class="row">
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/empl/beer.svg') ?>" alt="Beer" width="90px" height="90px">
                    <h4>La culture</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/empl/money.svg') ?>" alt="Money" width="90px" height="90px">
                    <h4>La fiscalité</h4>
                </div>
                <div class="col s4 center">
                    <img src="<?= base_url('assets/img/empl/target.svg') ?>" alt="Objective" width="90px" height="90px">
                    <h4>L'étranger</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <h3>Nos locaux</h3>
            <div class="row">
                <div class="col s12">
                    <ul class="tabs">
                        <li class="tab col s3"><a class="active" href="#l1">Local 1</a></li>
                        <li class="tab col s3"><a href="#l2">Local 2</a></li>
                        <li class="tab col s3"><a href="#l3">Local 3</a></li>
                    </ul>
                </div>
                <div id="l1" class="col s12 inner-tab">
                    <div class="col">
                        <img src="<?= base_url('assets/img/empl/loc1.png') ?>" alt="Loc1">
                    </div>
                    <div class="col">
                        <table>
                            <tr>
                                <th>Addresse</th><td>12, Camden Road, Dublin 8</td>
                            </tr>
                            <tr>
                                <th>Surface</th><td>26m2</td>
                            </tr>
                            <tr>
                                <th>Prix</th><td>2.100€ / mois</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="l2" class="col s12 inner-tab">
                    <div class="col">
                        <img src="<?= base_url('assets/img/empl/loc2.png') ?>" alt="Loc2">
                    </div>
                    <div class="col">
                        <table>
                            <tr>
                                <th>Addresse</th><td>Pearse Street, Dublin 2</td>
                            </tr>
                            <tr>
                                <th>Surface</th><td>56m2</td>
                            </tr>
                            <tr>
                                <th>Prix</th><td>Négociable</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="l3" class="col s12 inner-tab">
                    <div class="col">
                        <img src="<?= base_url('assets/img/empl/loc3.png') ?>" alt="Loc3">
                    </div>
                    <div class="col">
                        <table>
                            <tr>
                                <th>Addresse</th><td>Portview House, Thorncastle street, Dublin 4</td>
                            </tr>
                            <tr>
                                <th>Surface</th><td>181m2</td>
                            </tr>
                            <tr>
                                <th>Prix</th><td>5.800€ / mois</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h3>Comparatif</h3>
            <table class="bordered">
                <thead>
                    <tr><th></th><th>1er Local</th><th>2e Local</th><th>3e Local</th></tr>
                </thead>
                <tbody>
                    <tr><th>Prix</th><td>2100€</td><td>négociable</td><td>5800€</td></tr>
                    <tr><th>Surface</th><td>26m2</td><td>56m2</td><td>181m2</td></tr>
                    <tr><th>Emplacement</th><td>Proche centre ville</td><td>Coeur technologique de la ville</td><td>Proche centre ville</td></tr>
                    <tr><th>Petit plus</th><td></td><td>Histoire du batiment</td><td>Vue sur la Liffey</td></tr>
                </tbody>
            </table>
        </div>
    </div>

<?php
$data['load'] = array('jquery','materialize', 'tabs');
$this->load->view('utilities/footer',$data);

